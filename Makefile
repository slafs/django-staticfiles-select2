.PHONY: clean-pyc clean-build clean

help:
	@echo "clean - remove artifacts"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "update - update select2 to a version that's in SELECT2_VERSION variable"
	@echo "release - package and upload a release"
	@echo "sdist - package"
	@echo "bdist - package"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -type f -exec rm -f {} +
	find . -name '*.pyo' -type f -exec rm -f {} +
	find . -name '*~' -type f -exec rm -f {} +

test-release: clean
	python setup.py sdist bdist_wheel upload -r test

release: clean
	python setup.py sdist bdist_wheel upload

sdist: clean
	python setup.py sdist
	ls -l dist

bdist: clean
	python setup.py bdist_wheel
	ls -l dist

uninstall-select2:
	bower uninstall select2

commit-select2:
	git add staticfiles_select2/static/staticfiles_select2/select2/
	git commit staticfiles_select2/static/staticfiles_select2/select2/ -m "updated static files for select2 v.$$SELECT2_VERSION"

install-select2:
	bower install "select2#$$SELECT2_VERSION"
	bower uninstall jquery -f

update-select2:	uninstall-select2 install-select2 commit-select2

update-version:
	bumpversion --new-version "$$SELECT2_VERSION" patch

update: check-env clean update-select2 update-version

check-env:
	@echo $$SELECT2_VERSION
